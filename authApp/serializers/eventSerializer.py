from rest_framework import serializers
from authApp.models.event import Event

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ['id','organizer','tema', 'mensaje', 'fecha', 'lugar','image_event']

    def create(self, validated_data):
        eventInstance = Event.objects.create(**validated_data)
        return eventInstance

    def to_representation(self, obj):
        event = Event.objects.get(id = obj.id)
        return {
            'id' : event.id,
            'organizer': event.organizer,
            'tema' : event.tema,
            'mensaje' : event.mensaje,
            'fecha' : event.fecha,
            'lugar' : event.lugar,
            'image_event': event.image_event
        }