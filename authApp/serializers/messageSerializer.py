from rest_framework import serializers
from authApp.models.message import Message

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['id','escribe','fecha_mensaje','texto_mensaje']

    def create (self, validated_data):
        messageInstance = Message.objects.create(**validated_data)
        return messageInstance

    def to_representation(self, obj):
        message = Message.objects.get(id = obj.id)
        return{
            'id' : message.id,
            'escribe':message.escribe,
            'fecha_mensaje':message.fecha_mensaje,
            'texto_mensaje': message.texto_mensaje
            
        }