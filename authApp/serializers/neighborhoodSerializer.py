from rest_framework import serializers
from authApp.models.neighborhood import Neighborhood

class NeighborhoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Neighborhood
        fields = ['id','neighbor','name_neighborhood','address','city', 'country']

    def create (self, validated_data):
        neighborhoodInstance = Neighborhood.objects.create(**validated_data)
        return neighborhoodInstance

    def to_representation(self, obj):
        neighborhood = Neighborhood.objects.get(id = obj.id)
        return{
            'id' : neighborhood.id,
            'neighbor':neighborhood.neighbor,
            'name_neighborhood':neighborhood.name_neighborhood,
            'address': neighborhood.address,
            'city':neighborhood.city,
            'country':neighborhood.country            
        }