from authApp.models.user import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'celular', 'name','email','fechaNacimiento']

    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
            'id' : user.id,
            'username' : user.username,
            'password' : user.password,
            'celular' : user.celular,
            'name' : user.name,
            'email' : user.email,
            'fechaNacimiento' : user.fechaNacimiento
            
        }
    #metodo get
    def get_element(self, **obj):        
        return User.objects.filter(**obj)

    def delete_element(self, id):
        user = User.objects.get(id=id)
        user.delete()
        return {                     
            'id' : user.id,
            'username' : user.username,
            'password' : user.password,
            'celular' : user.celular,
            'name' : user.name,
            'email' : user.email,
            'fechaNacimiento' : user.fechaNacimiento
                                     
        }