from rest_framework import serializers
from authApp.models.comment import Comment

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['id','wcomment','publication','comentario','fecha_comment']

    def create (self, validated_data):
        commentInstance = Comment.objects.create(**validated_data)
        return commentInstance

    def to_representation(self, obj):
        comment = Comment.objects.get(id = obj.id)
        return{
            'id' : comment.id,
            'wcomment':comment.wcomment,
            'publication': comment.publication,
            'comentario': comment.comentario, 
            'fecha_comment': comment.fecha_comment           
        }