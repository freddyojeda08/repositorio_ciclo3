from rest_framework import serializers
from authApp.models.publication import Publication
from django.utils import timezone
import datetime

class PublicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publication
        fields = ['id','idusuario','image','description','fecha_publicacion']

    def create (self, validated_data):
        publicationInstance = Publication.objects.create(fecha_publicacion= datetime.datetime.now(tz=timezone.utc),**validated_data)
        return publicationInstance

    def to_representation(self, obj):
        publication = Publication.objects.get(id = obj.id)
        return{
            'id' : publication.id,
            'image' : publication.image,
            'description' : publication.description,
            'fecha_publicacion':publication.fecha_publicacion
        }
    
    def update(self, instance, validated_data):
        print(validated_data)
        return super().update(instance, validated_data)

    #metodo get
    def get_element(self, **obj):        
        return Publication.objects.filter(**obj)

    def delete_element(self, id):
        publication = Publication.objects.get(id=id)
        publication.delete()
        return {                     
            'id' : publication.id,
            'image' : publication.image,
            'description' : publication.description,
            'fecha_publicacion':publication.fecha_publicacion                    
        }