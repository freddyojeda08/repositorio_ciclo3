from django.db import models
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

class Message(models.Model):
    id= models.AutoField(primary_key=True)
    escribe= models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    fecha_mensaje= models.DateTimeField(auto_now_add=True)
    texto_mensaje= models.TextField(blank=True, null=True, max_length=3000)
    is_active= models.BooleanField(default=True)



