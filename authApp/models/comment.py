from django.db import models
from django.db import models
from django.contrib.auth.models import User
from .publication import Publication
from django.conf import settings

class Comment(models.Model):
    id = models.AutoField(primary_key=True)
    wcomment = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    publication= models.ForeignKey(Publication, related_name='comentar_post', on_delete=models.CASCADE)
    comentario=models.TextField(blank=True, null=True, max_length=3000)
    fecha_comment=models.DateTimeField(auto_now_add=True) 
    is_active= models.BooleanField(default=True)
    