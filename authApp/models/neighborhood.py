from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

class Neighborhood(models.Model):
    id= models.AutoField(primary_key=True)
    neighbor=models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    name_neighborhood= models.CharField(max_length=100)
    address= models.CharField(max_length=150)
    city= models.CharField(max_length=150)
    country= models.CharField(max_length=150)
    is_active= models.BooleanField(default=True)
