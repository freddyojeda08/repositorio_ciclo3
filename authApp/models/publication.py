from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.conf import settings 


class Publication(models.Model,):
    id = models.AutoField(primary_key=True)
    idusuario= models.ForeignKey (settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    image = models.CharField(blank=True, null=True, max_length=100)
    description = models.TextField(blank=True, null=True, max_length=2000)
    fecha_publicacion= models.CharField(blank=True, null=True, max_length=100)
    is_active= models.BooleanField(default=True)

    def save(self, **kwargs):
        super().save(**kwargs)