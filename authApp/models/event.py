from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

class Event(models.Model):
    id = models.AutoField(primary_key=True)
    organizer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    tema=models.CharField('Tema',max_length=20)
    mensaje=models.TextField(blank=True, null=True, max_length=3000)
    fecha=models.DateTimeField(auto_now_add=True)
    lugar=models.CharField(max_length=100)
    image_event= models.ImageField(upload_to= 'image', blank=True, null=True)
    is_active= models.BooleanField(default=True)
