from django.contrib import admin
from .models.user import User
from .models.event import Event
from .models.publication import Publication
from .models.comment import Comment
from .models.message import Message
from .models.neighborhood import Neighborhood

admin.site.register(User)
admin.site.register(Event)
admin.site.register(Publication)
admin.site.register(Comment)
admin.site.register(Message)
admin.site.register(Neighborhood)

