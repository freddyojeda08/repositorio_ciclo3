from django.conf import settings 
from rest_framework import generics, status, views 
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework.permissions import IsAuthenticated 
 
from authApp.models.publication import Publication 
from authApp.serializers.publicationSerializer import PublicationSerializer
 
class PublicationAllDetail(views.APIView):     
    queryset = Publication.objects.all()     
    serializer_class = PublicationSerializer
    permission_classes = (IsAuthenticated,)   

    def get(self, request, *args, **kwargs):
        #debe llegar PK del publication y su access token
        #RETURN JSON con la data de todos las publicaciones

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        """
        if valid_data['publication_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        """
        publicationSerializer = PublicationSerializer()
        query_result = publicationSerializer.get_element()

        result = []
        for publication in query_result:
            result.append(publicationSerializer.to_representation(publication))

        return Response(result, status=status.HTTP_200_OK) 