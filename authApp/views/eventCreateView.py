from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from authApp.serializers.eventSerializer import EventSerializer


class EventCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = EventSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        
        return Response(serializer.validated_data, status=status.HTTP_201_CREATED)