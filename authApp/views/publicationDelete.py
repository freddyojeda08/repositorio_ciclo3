from django.conf import settings 
from rest_framework import generics, serializers, status, views
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework.permissions import IsAuthenticated 

from authApp.models.publication import Publication
from authApp.serializers.publicationSerializer import PublicationSerializer

class PublicationDelete(views.APIView):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        #debe llegar PK del user y su accen token
        #mensaje de que se elimino el usuario solicitado

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        """
        if valid_data['publication_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        """
        publicationSerializer = PublicationSerializer()

        stringResponse = {'detail':'se elimino correctamente'}
        return Response(publicationSerializer.delete_element(id=kwargs["pk"]), status=status.HTTP_200_OK)