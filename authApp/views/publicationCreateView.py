from rest_framework import status, views
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from authApp.serializers.publicationSerializer import PublicationSerializer

class PublicationCreateView(views.APIView):

    def post(self, request, *arg, **kwargs):

        serializer = PublicationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        p = serializer.save()

        """
        tokenData = {"image":request.data["image"],"description":request.data["description"]}
        tokenserializer = TokenObtainPairSerializer(data=tokenData)
        tokenserializer.is_valid(raise_exception=True)"""

        return Response(serializer.to_representation(p), status=status.HTTP_201_CREATED)
