from .eventCreateView import EventCreateView

from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .userAllDetail import UserAllDetail
from .userDelete import UserDelete
from .userUpdate import UserUpdate

from .publicationCreateView import PublicationCreateView
from .publicationAllDetail import PublicationAllDetail
from .publicationDelete import PublicationDelete
from .publicationDetail import PublicationDetailView
from .publicationUpdate import PublicationUpdate