from django.conf import settings 
from rest_framework import generics, status, views 
from rest_framework.response import Response 
from rest_framework_simplejwt.backends import TokenBackend 
from rest_framework.permissions import IsAuthenticated 
 
from authApp.models.publication import Publication
from authApp.serializers.publicationSerializer import PublicationSerializer
 
class PublicationUpdate(generics.RetrieveAPIView):     
    queryset = Publication.objects.all()     
    serializer_class = PublicationSerializer     
    permission_classes = (IsAuthenticated,)   

    def put(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        """
        if valid_data['publication_id'] != kwargs['pk']:
            stringResponse = {'detail':'No está autorizado para ver esto'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)                
        """
        p = Publication( id = kwargs['pk'])

        
        publicationSerializer = PublicationSerializer(p, data=request.data)
        publicationSerializer.is_valid(raise_exception=True)        
        updatedPublication= publicationSerializer.save()        

        return Response(publicationSerializer.to_representation(updatedPublication), status=status.HTTP_200_OK)